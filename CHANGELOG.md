# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 1.5.3

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 1.5.2

- patch: Internal maintenance: Improve fetch build data.

## 1.5.1

- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: Update tests to authenticate only via bitbucket access token.

## 1.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 1.2.0

- minor: Add support for token based authentication.
- patch: Internal maintenance: update pipe versions in pipeline config file.

## 1.1.1

- patch: Update readme. Add details about BITBUCKET_APP_PASSWORD permissions.

## 1.1.0

- minor: Add support for OUTPUT_FILE_FORMAT variable. Added support for JSON output file.
- minor: Migrate base dependency to the original prettytable.
- patch: Internal maintenance: Bump base docker image to python:3.10-slim.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Bump version of the bitbucket-pipe-release.
- patch: Internal maintenance: Update the communty link.

## 1.0.1

- patch: Internal maintenance: Update docker image in Dockerfile to fix issue with tests.
- patch: Internal maintenance: Update package versions in requirements and test requirements.
- patch: Internal maintenance: Update pipe version of bitbucket-pipe-release.

## 1.0.0

- major: Update 'Build Duration' field to display the actual build duration (includes build duration for self-hosted runner), and add 'Build Minutes used' field to display build minutes consumed from the workspace's plan

## 0.2.2

- patch: Internal maintenance: Refactor unit tests.

## 0.2.1

- patch: Fixed date and time format in the build duration.

## 0.2.0

- minor: Fixed workspace name in the README.md YAML fragments.

## 0.1.0

- minor: Initial release.
