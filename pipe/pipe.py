import os
import datetime
from datetime import date

import yaml
import requests
import dateutil.parser
from prettytable import PrettyTable

from bitbucket_pipes_toolkit import Pipe


today = date.today().strftime("%m_%d_%Y")

BITBUCKET_API_BASE_URL = "https://api.bitbucket.org/2.0"

schema = {
    'BITBUCKET_USERNAME': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
    'BITBUCKET_APP_PASSWORD': {'required': False, 'type': 'string', 'excludes': 'BITBUCKET_ACCESS_TOKEN'},
    'BITBUCKET_ACCESS_TOKEN': {'required': False, 'type': 'string', 'excludes': ['BITBUCKET_USERNAME', 'BITBUCKET_APP_PASSWORD']},
    'WORKSPACE': {'type': 'string', 'required': False, 'nullable': True, 'default': os.getenv('BITBUCKET_WORKSPACE')},
    'REPO_LIST': {'type': 'string', 'required': False, 'default': ""},
    'FILENAME': {'type': 'string', 'required': False,
                 'default': f"build_usage_{today}_{os.getenv('BITBUCKET_BUILD_NUMBER')}.txt"},
    'OUTPUT_FILE_FORMAT': {'type': 'string', 'required': False, 'allowed': ['json', 'table'], 'default': 'table'},
    'BUILD_DAYS': {'type': 'number', 'required': False, 'default': 30},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False},
}


class BuildStatisticsPipe(Pipe):
    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )

        self.bitbucket_workspace = self.get_variable('WORKSPACE')
        self.bitbucket_repo_list = self.get_variable('REPO_LIST')
        self.filename = self.get_variable('FILENAME')
        self.output_file_format = self.get_variable('OUTPUT_FILE_FORMAT')
        self.build_days = self.get_variable('BUILD_DAYS')
        self.start_date = None
        self.end_date = None
        self.debug = self.get_variable('DEBUG')
        self.auth = self.resolve_auth()

    def run(self):
        super().run()

        self.log_info("Executing the pipe...")
        self.log_info(f"Bitbucket workspace: {self.bitbucket_workspace}")
        self.log_info(f"Bitbucket repository list: {self.bitbucket_repo_list}")
        self.log_info(f"File Name: {self.filename}")
        self.log_info(f"Output file format: {self.output_file_format}")
        self.log_info(f"Build days: {self.build_days}")
        self.log_info(f"Debug: {self.debug}")

        self.end_date = datetime.datetime.now(datetime.timezone.utc)
        self.start_date = self.end_date - datetime.timedelta(self.build_days)
        self.log_debug(f"Build statistics period of time: from {self.start_date.isoformat()} to {self.end_date.isoformat()}")

        # Get the repository list based on the value specified in WORKSPACE and REPO_LIST
        self.get_repo_list()

        repo_pipeline_details = []
        # Get Pipeline details for the repositories
        for repo in self.bitbucket_repo_list:
            repo_data = self.get_repo_pipeline_summary(repo)
            repo_pipeline_details.append(repo_data)

        # Generate the table
        prettyTable = PrettyTable()
        prettyTable.field_names = ["Repository", "Builds", "Build Duration", "Build Minutes Used"]
        prettyTable.align["Repository"] = "l"
        prettyTable.align["Builds"] = "c"
        prettyTable.align["Build Duration"] = "c"
        prettyTable.align["Build Minutes Used"] = "c"
        prettyTable.sortby = "Build Minutes Used"
        prettyTable.reversesort = True

        # Feed the table
        for item in repo_pipeline_details:
            prettyTable.add_row([item["Repository"], item["Builds"], item["Build Duration"], item["Build Minutes Used"]])

        # ASCII table to logs
        print(prettyTable.get_string(title="Bitbucket build statistics"))

        # Generate the file to capture output
        output_filename = self.filename
        if self.output_file_format.lower() == 'json':
            output_filename = f"{output_filename}.json"
            output_content = prettyTable.get_json_string()
        else:
            # ASCII table by default
            output_content = prettyTable.get_string(title="Repository build details")

        with open(output_filename, 'w') as f:
            f.write(output_content)

        self.log_info(f"Successfully created file: {output_filename}")
        self.success(message="Success!")

    def get_repo_list(self):
        """
        Retrieves repository list
        :return: None
        """
        self.bitbucket_repo_list = self.bitbucket_repo_list.split()
        if not self.bitbucket_repo_list:
            # Request 100 repositories per page
            url = f"{BITBUCKET_API_BASE_URL}/repositories/{self.bitbucket_workspace}?pagelen=100&fields=next,values.full_name"

            # Keep fetching pages while there's a page to fetch
            while url is not None:
                try:
                    response = requests.get(url, auth=self.auth)
                    response.raise_for_status()
                except requests.exceptions.RequestException as e:
                    self.fail(f"{e}")
                response = response.json()
                for repo in response['values']:
                    reponame = repo['full_name']
                    self.bitbucket_repo_list.append(reponame)
                # Get the next page URL, if present
                url = response.get('next', None)
        else:
            for index in range(len(self.bitbucket_repo_list)):
                repo = self.bitbucket_repo_list[index]
                repo_full_name = repo if "/" in repo else f"{self.bitbucket_workspace}/{repo}"
                self.bitbucket_repo_list[index] = repo_full_name

    def get_repo_pipeline_summary(self, repository_path):
        """
        Retrieves pipelines summary of a repository
        :param repository_path: Repository path (ex: account/repo)
        :return: Pipeline summary for the repository
        """
        result = self.get_repo_pipelines(repository_path)

        build_seconds_used = 0
        build_duration = 0
        build_count = 0
        for r in result:
            build_date = dateutil.parser.parse(r['created_on'])
            if build_date > self.start_date:
                build_seconds_used += r['build_seconds_used']
                build_duration += r['duration_in_seconds']
                build_count += 1

        detail = {"Repository": repository_path, "Builds": build_count,
                  "Build Duration": self.convert_seconds_to_datetime(build_duration),
                  "Build Minutes Used": self.convert_seconds_to_datetime(build_seconds_used)}

        return detail

    @staticmethod
    def convert_seconds_to_datetime(seconds):
        """
        Returns number of days, hours, minutes and seconds from the number of seconds
        :param seconds: number of seconds to convert
        :return: Coverted number of days, hours, minutes and seconds
        """
        days = seconds // (24 * 3600)
        seconds = seconds % (24 * 3600)
        hours = seconds // 3600
        seconds %= 3600
        minutes = seconds // 60
        seconds %= 60
        seconds = seconds

        return "{0}d {1}h {2}m {3}s".format(days, hours, minutes, seconds)

    def get_repo_pipelines(self, repository_path):
        """
        Retrieves pipelines list
        :param repository_path: Repository path (ex: account/repo)
        :return: List of pipelines information in JSON format
        """
        url = f"{BITBUCKET_API_BASE_URL}/repositories/{repository_path}/pipelines/"
        page = 1
        pipelines = []
        # Keep fetching pages while there's a page to fetch
        while True:
            try:
                params = {
                    'sort': "-created_on",
                    'page': page,
                    'pagelen': 100
                }
                response = requests.get(url, auth=self.auth, params=params)
                response.raise_for_status()
            except requests.exceptions.RequestException as e:
                self.fail(f"{e}")

            response = response.json()
            pipelines.extend(response['values'])

            if response['pagelen'] != 100:
                break

            # stop fetch new data
            if response.get('values'):
                build_date = dateutil.parser.parse(response['values'][-1]['created_on'])
                if build_date < self.start_date:
                    break

            page += 1

        return pipelines


if __name__ == '__main__':
    with open('/pipe.yml', 'r') as metadata_file:
        metadata = yaml.safe_load(metadata_file.read())

    pipe = BuildStatisticsPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
