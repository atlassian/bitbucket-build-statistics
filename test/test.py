import os

from bitbucket_pipes_toolkit.test import PipeTestCase


class BuildStatisticsTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()

    def test_default_success_token(self):
        # default current workspace will be taken
        result = self.run_container(environment={
            "BITBUCKET_ACCESS_TOKEN": os.getenv("BITBUCKET_ACCESS_TOKEN"),
            "WORKSPACE": os.getenv("WORKSPACE", "bbcitest"),
            "REPO_LIST": os.getenv("REPO_LIST", "test-trigger-build")
        })
        self.assertIn('Successfully created file', result)
