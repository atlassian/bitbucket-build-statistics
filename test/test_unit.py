from copy import copy
import datetime
import glob
import os
import sys
from unittest import mock

from bitbucket_pipes_toolkit.test import PipeTestCase
from pipe.pipe import BuildStatisticsPipe, schema


class BitbucketBuildStatisticTestCase(PipeTestCase):
    @classmethod
    def setUpClass(cls):
        cls.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    @classmethod
    def tearDownClass(cls):
        sys.path = cls.sys_path

    def tearDown(self):
        for filename in glob.glob(f'{os.getcwd()}/build_usage_*'):
            os.remove(filename)

    @staticmethod
    def get_date():
        return str(
            datetime.datetime.now(datetime.timezone.utc) - datetime.timedelta(int(os.getenv('BUILD_DAYS', 30)) - 1)
        )

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'WORKSPACE': '', 'REPO_LIST': '', 'BUILD_DAYS': '5'})
    @mock.patch('requests.get')
    def test_default(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [{'values': [{'full_name': 'account/repo_1'}]},
                                                            {'values': [{'created_on': self.get_date(),
                                                                         'build_seconds_used': 325,
                                                                         'duration_in_seconds': 325}],
                                                             'pagelen': 1}
                                                            ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()
        filename = glob.glob(f'{os.getcwd()}/build_usage_*.txt')[0]
        with open(filename) as f:
            actual_build_usage = f.read()

        self.assertRegex(actual_build_usage,
                         r'Repository [^\d]+ Builds [^\d]+ Build Duration [^\d]+ Build Minutes Used')
        self.assertRegex(actual_build_usage, r'account\/repo_1 [^\d]+ 1 [^\d]+ 0d 0h 5m 25s [^\d]+ 0d 0h 5m 25s')

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'REPO_LIST': 'myaccount/myrepo'})
    @mock.patch('requests.get')
    def test_with_repo_full_name_passed(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [{'values': [{'created_on': self.get_date(),
                                                                         'build_seconds_used': 3600,
                                                                         'duration_in_seconds': 3600}],
                                                             'pagelen': 1}
                                                            ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()

        filename = glob.glob(f'{os.getcwd()}/build_usage_*.txt')[0]
        with open(filename) as f:
            actual_build_usage = f.read()

        self.assertRegex(actual_build_usage,
                         r'Repository [^\d]+ Builds [^\d]+ Build Duration [^\d]+ Build Minutes Used')
        self.assertRegex(actual_build_usage, r'myaccount\/myrepo [^\d]+ 1 [^\d]+ 0d 1h 0m 0s [^\d]+ 0d 1h 0m 0s')

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'WORKSPACE': 'myaccount', 'REPO_LIST': 'myrepo'
                                  })
    @mock.patch('requests.get')
    def test_with_workspace_passed(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [{'values': [{'created_on': self.get_date(),
                                                                         'build_seconds_used': 2799,
                                                                         'duration_in_seconds': 2799}],
                                                             'pagelen': 1}
                                                            ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()

        filename = glob.glob(f'{os.getcwd()}/build_usage_*.txt')[0]
        with open(filename) as f:
            actual_build_usage = f.read()

        self.assertRegex(actual_build_usage,
                         r'Repository [^\d]+ Builds [^\d]+ Build Duration [^\d]+ Build Minutes Used')
        self.assertRegex(actual_build_usage, r'myaccount\/myrepo [^\d]+ 1 [^\d]+ 0d 0h 46m 39s [^\d]+ 0d 0h 46m 39s')

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'WORKSPACE': 'myaccount', 'REPO_LIST': 'myrepo'
                                  })
    @mock.patch('requests.get')
    def test_with_large_seconds_number(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [{'values': [{'created_on': self.get_date(),
                                                                         'build_seconds_used': 200000,
                                                                         'duration_in_seconds': 200000}],
                                                             'pagelen': 1}
                                                            ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()

        filename = glob.glob(f'{os.getcwd()}/build_usage_*.txt')[0]
        with open(filename) as f:
            actual_build_usage = f.read()

        self.assertRegex(actual_build_usage,
                         r'Repository [^\d]+ Builds [^\d]+ Build Duration [^\d]+ Build Minutes Used')
        self.assertRegex(actual_build_usage, r'myaccount\/myrepo [^\d]+ 1 [^\d]+ 2d 7h 33m 20s [^\d]+ 2d 7h 33m 20s')

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'WORKSPACE': 'myaccount', 'REPO_LIST': 'myrepo'
                                  })
    @mock.patch('requests.get')
    def test_with_2x_memory_build(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [{'values': [{'created_on': self.get_date(),
                                                                         'build_seconds_used': 2800,
                                                                         'duration_in_seconds': 1400}],
                                                             'pagelen': 1}
                                                            ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()

        filename = glob.glob(f'{os.getcwd()}/build_usage_*.txt')[0]
        with open(filename) as f:
            actual_build_usage = f.read()

        self.assertRegex(actual_build_usage,
                         r'Repository [^\d]+ Builds [^\d]+ Build Duration [^\d]+ Build Minutes Used')
        self.assertRegex(actual_build_usage, r'myaccount\/myrepo [^\d]+ 1 [^\d]+ 0d 0h 23m 20s [^\d]+ 0d 0h 46m 40s')

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'WORKSPACE': 'myaccount', 'REPO_LIST': 'myrepo',
                                  'FILENAME': 'build_usage_custom_file.txt'})
    @mock.patch('requests.get')
    def test_with_custom_filename_text_passed(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [
            {'values': [
                {'created_on': self.get_date(),
                 'build_seconds_used': 100,
                 'duration_in_seconds': 100
                 },
            ],
                'pagelen': 1
            }
        ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()

        self.assertTrue(os.path.exists('build_usage_custom_file.txt'))

    @mock.patch.dict(os.environ, {'BITBUCKET_ACCESS_TOKEN': 'test',
                                  'WORKSPACE': 'myaccount', 'REPO_LIST': 'myrepo',
                                  'FILENAME': 'build_usage_custom_file', 'OUTPUT_FILE_FORMAT': 'json'})
    @mock.patch('requests.get')
    def test_with_custom_filename_json_passed(self, repos_details_mock):
        repos_details_mock.return_value.json.side_effect = [
            {'values': [
                {'created_on': self.get_date(),
                 'build_seconds_used': 100,
                 'duration_in_seconds': 100
                 },
            ],
                'pagelen': 1
            }
        ]
        repos_details_mock.raise_for_status = mock.Mock()
        BuildStatisticsPipe(schema=schema, check_for_newer_version=True).run()

        self.assertTrue(os.path.exists('build_usage_custom_file.json'))
